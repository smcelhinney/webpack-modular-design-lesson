import React from 'react';
import {render} from 'react-dom';
import Header from 'javascripts/components/header';
import BodyContent from 'javascripts/components/body';
import styles from 'stylesheets/main';

class App extends React.Component {
  render() {
    return <div className={styles.appMountPoint}>
      <Header/>
      <BodyContent/>
    </div>;
  }
}

const mountPoint = document.createElement('div');
render(
  <App/>, mountPoint);

document.body.appendChild(mountPoint);

setTimeout(() => {
  mountPoint.childNodes[0].classList.add(styles.active);
}, 500);

// Remove the app mount point.
if (module.hot) {
  module.hot.dispose(() => {
    document.body.removeChild(mountPoint);
  });
}

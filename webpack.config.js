'use strict';

const webpack = require('webpack');
const path = require('path');

const autoprefixer = require('autoprefixer')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const isProd = process.env.NODE_ENV === 'production';
let classStructure = '[path]___[name]__[local]___[hash:base64:5]';

module.exports = {
  context: __dirname,
  output: {
    path: path.resolve(__dirname, './build'),
    filename: '[name].js'
  },
  module: {
    loaders: [{
      test: /\.jsx?/,
      use: ['babel-loader?presets[]=es2015&presets[]=react'],
    }, {
      test: /\.sass$/,
      get use() {
        if (isProd) {
          return ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [

              `css-loader?importLoaders=1&modules&localIdentName=${classStructure}
              `,
              'postcss-loader',
              'sass-loader'
            ]
          })
        }
        return [
          'style-loader',
          `css-loader?importLoaders=1&modules&localIdentName=${classStructure}`,
          'postcss-loader',
          'sass-loader',
        ]
      },
    }]
  },
  entry: {
    app: './app.js'
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new ExtractTextPlugin('[name].css'),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer({
            browsers: [
              'last 3 version',
              'ie >= 10',
            ],
          }),
        ]
      },
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.sass'],
    modules: [path.resolve(__dirname, './src'), 'node_modules']
  }
};

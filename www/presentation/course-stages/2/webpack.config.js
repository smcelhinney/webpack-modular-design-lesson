'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = {
  context: __dirname,
  entry: './app.js',
  plugins: [
    new webpack.NamedModulesPlugin()
  ],
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'app.js'
  },
  module: {
    loaders: [{
        test: /\.jsx?/,
        use: ['babel-loader?presets[]=es2015&presets[]=react'],
      },
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1&modules&localIdentName=[name]-[local]',
          'sass-loader',
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.sass'],
    modules: [path.resolve(__dirname, './src'), 'node_modules']
  }
};

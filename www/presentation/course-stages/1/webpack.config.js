'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = {
  context: __dirname,
  entry: './app.js',
  plugins: [
    new webpack.NamedModulesPlugin(),
  ],
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'app.js'
  },
  module: {
    loaders: [{
      test: /\.jsx?/,
      use: ['babel-loader?presets[]=es2015&presets[]=react'],
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, './src'), 'node_modules']
  }
};

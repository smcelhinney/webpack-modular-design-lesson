import React from 'react';
import {render} from 'react-dom';
import HelloWorld from 'javascripts/components/hello-world/component';

class App extends React.Component {
  render() {
    return <HelloWorld />;
  }
}

const mountPoint = document.getElementById('mountPoint');
render(<App/>, mountPoint);

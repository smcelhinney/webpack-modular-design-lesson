'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';

let config = {
  context: __dirname,
  entry: './app.js',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'app.js'
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    new webpack.NamedModulesPlugin()
  ],
  module: {
    loaders: [{
        test: /\.jsx?/,
        use: ['babel-loader?presets[]=es2015&presets[]=react'],
      },
      {
        test: /\.sass$/,
        get use() {
          if (isProd) {
            return ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: [
                'css-loader?importLoaders=1&modules&localIdentName=[name]-[local]',
                'sass-loader',
              ]
            })
          }
          return [
            'style-loader',
            'css-loader?importLoaders=1&modules&localIdentName=[name]-[local]',
            'sass-loader',
          ];
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.sass'],
    modules: [path.resolve(__dirname, './src'), 'node_modules']
  }
};


// console.log(config);
// process.exit();
module.exports = config;

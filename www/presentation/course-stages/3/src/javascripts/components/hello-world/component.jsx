import React from 'react';
import helloWorldStyles from './hw';

export default class HelloWorld extends React.Component {
  render() {
    return <p className={helloWorldStyles.paragraphClass}>Hello World!</p>;
  }
}

require('./src/javascripts/index');

// This tells HMR to replace all the artefacts in this file if and when required
// and what to do if it fails to replace anything.

if(module.hot) {
	module.hot.accept(function(err) {
		if(err) {
			console.error('Cannot apply hot update', err);
		}
	});
}

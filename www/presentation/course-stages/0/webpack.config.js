'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = {
  context: __dirname,
  entry: './app.js',
  plugins: [
    new webpack.NamedModulesPlugin(),
  ],
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'app.js'
  },
  resolve: {
    extensions: ['.js']
  }
};
